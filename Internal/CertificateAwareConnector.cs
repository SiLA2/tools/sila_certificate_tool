﻿using SiLACertificateGenerator.ViewModels;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Discovery;

namespace SiLACertificateGenerator.Internal
{
    public class CertificateAwareConnector : ServerConnector
    {
        private readonly ConcurrentDictionary<Guid, ServerViewModel> _servers = new ConcurrentDictionary<Guid, ServerViewModel>();

        public CertificateAwareConnector(IClientExecutionManager executionManager) : base(executionManager)
        {
        }

        public ServerViewModel GetOrCreateViewModel(ServerData server)
        {
            if (_servers.TryGetValue(server.Config.Uuid, out var viewModel))
            {
                return viewModel;
            }
            else
            {
                viewModel = new ServerViewModel(server);
                if (!_servers.TryAdd(server.Config.Uuid, viewModel))
                {
                    return GetOrCreateViewModel(server);
                }
                return viewModel;
            }
        }

        protected override ServerData CheckCertificateAuthority(ServerData server, X509Certificate2 serverCertificate, X509Certificate2 certificateAuthority)
        {
            var serverModel = GetOrCreateViewModel(server);
            serverModel.Certificate = new CertificateViewModel(new ServerCertificate(server.Config.Uuid), serverCertificate);
            if (!serverCertificate.Extensions.OfType<X509SubjectAlternativeNameExtension>().Any())
            {
                serverModel.Problems.Add(new CertificateProblem("Certificate does not specify SAN", ProblemSeverity.Information));
            }
            return base.CheckCertificateAuthority(server, serverCertificate, certificateAuthority);
        }

        protected override ServerData ServerCertificateCouldNotBeRead(ServerData server)
        {
            GetOrCreateViewModel(server).Problems.Add(new CertificateProblem("Certificate could not be read", ProblemSeverity.Critical));
            return base.ServerCertificateCouldNotBeRead(server);
        }

        protected override ServerData ServerUuidDoesNotMatchCertificate(ServerData server, Guid serverIdFromCertificate, X509Certificate2 serverCertificate)
        {
            GetOrCreateViewModel(server).Problems.Add(new CertificateProblem($"Server reports UUID {server.Config.Uuid} but certificate contains {serverIdFromCertificate}.", ProblemSeverity.Warning));
            return base.ServerUuidDoesNotMatchCertificate(server, serverIdFromCertificate, serverCertificate);
        }

        protected override ServerData ServerUuidDoesNotMatchDnsEntry(ServerData server, Guid dnsGuid)
        {
            GetOrCreateViewModel(server).Problems.Add(new CertificateProblem($"Server reports UUID {server.Config.Uuid} but DNS-SD entry is {dnsGuid}.", ProblemSeverity.Warning));
            return base.ServerUuidDoesNotMatchDnsEntry(server, dnsGuid);
        }

        protected override ServerData ServerUuidNotAnnounced(ServerData server)
        {
            GetOrCreateViewModel(server).Problems.Add(new CertificateProblem("Server does not announce used certificate through discovery.", ProblemSeverity.Information));
            return base.ServerUuidNotAnnounced(server);
        }

        protected override ServerData ServerUuidNotContainedInCertificate(ServerData server, X509Certificate2 serverCertificate)
        {
            GetOrCreateViewModel(server).Problems.Add(new CertificateProblem("Server certificate does not contain Server UUID.", ProblemSeverity.Warning));
            return base.ServerUuidNotContainedInCertificate(server, serverCertificate);
        }
    }
}
