﻿using Common.Logging;
using Common.Logging.Simple;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiLACertificateGenerator.Internal
{
    internal class DebugLogger : Common.Logging.ILoggerFactoryAdapter, ILog
    {
        public bool IsTraceEnabled => true;

        public bool IsDebugEnabled => true;

        public bool IsErrorEnabled => true;

        public bool IsFatalEnabled => true;

        public bool IsInfoEnabled => true;

        public bool IsWarnEnabled => true;

        public IVariablesContext GlobalVariablesContext => new NoOpVariablesContext();

        public IVariablesContext ThreadVariablesContext => new NoOpVariablesContext();

        public INestedVariablesContext NestedThreadVariablesContext => new NoOpNestedVariablesContext();

        public void Debug(object message)
        {
            System.Diagnostics.Debug.WriteLine(message);
        }

        public void Debug(object message, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(message);
            System.Diagnostics.Debug.WriteLine(exception);
        }

        public void Debug(Action<FormatMessageHandler> formatMessageCallback)
        {
            throw new NotImplementedException();
        }

        public void Debug(Action<FormatMessageHandler> formatMessageCallback, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            throw new NotImplementedException();
        }

        public void Debug(IFormatProvider formatProvider, Action<FormatMessageHandler> formatMessageCallback)
        {
            throw new NotImplementedException();
        }

        public void Debug(IFormatProvider formatProvider, Action<FormatMessageHandler> formatMessageCallback, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            throw new NotImplementedException();
        }

        public void DebugFormat(string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void DebugFormat(string format, Exception exception, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void DebugFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void DebugFormat(IFormatProvider formatProvider, string format, Exception exception, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void Error(object message)
        {
            System.Diagnostics.Debug.WriteLine(message);
        }

        public void Error(object message, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(message);
            System.Diagnostics.Debug.WriteLine(exception);
        }

        public void Error(Action<FormatMessageHandler> formatMessageCallback)
        {
            throw new NotImplementedException();
        }

        public void Error(Action<FormatMessageHandler> formatMessageCallback, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            throw new NotImplementedException();
        }

        public void Error(IFormatProvider formatProvider, Action<FormatMessageHandler> formatMessageCallback)
        {
            throw new NotImplementedException();
        }

        public void Error(IFormatProvider formatProvider, Action<FormatMessageHandler> formatMessageCallback, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            throw new NotImplementedException();
        }

        public void ErrorFormat(string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void ErrorFormat(string format, Exception exception, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void ErrorFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void ErrorFormat(IFormatProvider formatProvider, string format, Exception exception, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void Fatal(object message)
        {
            System.Diagnostics.Debug.WriteLine(message);
        }

        public void Fatal(object message, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(message);
            System.Diagnostics.Debug.WriteLine(exception);
        }

        public void Fatal(Action<FormatMessageHandler> formatMessageCallback)
        {
            throw new NotImplementedException();
        }

        public void Fatal(Action<FormatMessageHandler> formatMessageCallback, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            throw new NotImplementedException();
        }

        public void Fatal(IFormatProvider formatProvider, Action<FormatMessageHandler> formatMessageCallback)
        {
            throw new NotImplementedException();
        }

        public void Fatal(IFormatProvider formatProvider, Action<FormatMessageHandler> formatMessageCallback, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            throw new NotImplementedException();
        }

        public void FatalFormat(string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void FatalFormat(string format, Exception exception, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
            System.Diagnostics.Debug.WriteLine(exception);
        }

        public void FatalFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void FatalFormat(IFormatProvider formatProvider, string format, Exception exception, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
            System.Diagnostics.Debug.WriteLine(exception);
        }

        public ILog GetLogger(Type type)
        {
            return this;
        }

        public ILog GetLogger(string key)
        {
            return this;
        }

        public void Info(object message)
        {
            System.Diagnostics.Debug.WriteLine(message);
        }

        public void Info(object message, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(message);
            System.Diagnostics.Debug.WriteLine(exception);
        }

        public void Info(Action<FormatMessageHandler> formatMessageCallback)
        {
            throw new NotImplementedException();
        }

        public void Info(Action<FormatMessageHandler> formatMessageCallback, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            throw new NotImplementedException();
        }

        public void Info(IFormatProvider formatProvider, Action<FormatMessageHandler> formatMessageCallback)
        {
            throw new NotImplementedException();
        }

        public void Info(IFormatProvider formatProvider, Action<FormatMessageHandler> formatMessageCallback, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            throw new NotImplementedException();
        }

        public void InfoFormat(string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void InfoFormat(string format, Exception exception, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
            System.Diagnostics.Debug.WriteLine(exception);
        }

        public void InfoFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void InfoFormat(IFormatProvider formatProvider, string format, Exception exception, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
            System.Diagnostics.Debug.WriteLine(exception);
        }

        public void Trace(object message)
        {
            System.Diagnostics.Debug.WriteLine(message);
        }

        public void Trace(object message, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(message);
            System.Diagnostics.Debug.WriteLine(exception);
        }

        public void Trace(Action<FormatMessageHandler> formatMessageCallback)
        {
            throw new NotImplementedException();
        }

        public void Trace(Action<FormatMessageHandler> formatMessageCallback, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            throw new NotImplementedException();
        }

        public void Trace(IFormatProvider formatProvider, Action<FormatMessageHandler> formatMessageCallback)
        {
            throw new NotImplementedException();
        }

        public void Trace(IFormatProvider formatProvider, Action<FormatMessageHandler> formatMessageCallback, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            throw new NotImplementedException();
        }

        public void TraceFormat(string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void TraceFormat(string format, Exception exception, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
            System.Diagnostics.Debug.WriteLine(exception);
        }

        public void TraceFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void TraceFormat(IFormatProvider formatProvider, string format, Exception exception, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
            System.Diagnostics.Debug.WriteLine(exception);
        }

        public void Warn(object message)
        {
            System.Diagnostics.Debug.WriteLine(message);
        }

        public void Warn(object message, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(message);
            System.Diagnostics.Debug.WriteLine(exception);
        }

        public void Warn(Action<FormatMessageHandler> formatMessageCallback)
        {
            throw new NotImplementedException();
        }

        public void Warn(Action<FormatMessageHandler> formatMessageCallback, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            throw new NotImplementedException();
        }

        public void Warn(IFormatProvider formatProvider, Action<FormatMessageHandler> formatMessageCallback)
        {
            throw new NotImplementedException();
        }

        public void Warn(IFormatProvider formatProvider, Action<FormatMessageHandler> formatMessageCallback, Exception exception)
        {
            System.Diagnostics.Debug.WriteLine(exception);
            throw new NotImplementedException();
        }

        public void WarnFormat(string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void WarnFormat(string format, Exception exception, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
            System.Diagnostics.Debug.WriteLine(exception);
        }

        public void WarnFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void WarnFormat(IFormatProvider formatProvider, string format, Exception exception, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
            System.Diagnostics.Debug.WriteLine(exception);
        }
    }
}
