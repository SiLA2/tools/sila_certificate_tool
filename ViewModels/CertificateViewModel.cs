﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SiLACertificateGenerator.ViewModels
{
    public class CertificateViewModel : ViewModelBase
    {
        private DelegateCommand _createCommand;
        private DelegateCommand _exportCommand;

        public CertificateViewModel(CertificateMode mode, X509Certificate2? certificate)
        {
            _mode = mode;
            _certificate = certificate;
            _validTo = DateTime.UtcNow.AddYears(100);
            _cn = mode.DefaultCommonName;

            if (certificate != null )
            {
                ConsumeDN(certificate.Subject);
                _issuer = certificate.Issuer;
                _selfSigned = certificate.Issuer == certificate.Subject;
                _validFrom = certificate.NotBefore;
                _validTo = certificate.NotAfter;

                var altNames = certificate.Extensions.OfType<X509SubjectAlternativeNameExtension>().FirstOrDefault();
                if (altNames != null)
                {
                    foreach (var dnsName in altNames.EnumerateDnsNames())
                    {
                        AlternativeNames.Add(new AlternativeNameViewModel { Name = dnsName });
                    }

                    foreach (var ipAddress in altNames.EnumerateIPAddresses())
                    {
                        AlternativeNames.Add(new AlternativeNameViewModel { Name = ipAddress.ToString() });
                    }
                }
            }

            _createCommand = new DelegateCommand(Create);
            _exportCommand = new DelegateCommand(Export);
            _hasChanges = certificate == null;
        }

        public ObservableCollection<AlternativeNameViewModel> AlternativeNames { get; } = new ObservableCollection<AlternativeNameViewModel>();

        private void Export()
        {
            SaveCertificate(false);
        }

        public ICommand CreateCommand => _createCommand;

        public ICommand ExportCommand => _exportCommand;

        private void ConsumeDN(string distinguishedName)
        {
            if (distinguishedName == null) { return; }

            var components = distinguishedName.Split(',');
            foreach ( var component in components )
            {
                var subComponents = component.Split('=');
                if (subComponents.Length != 2) 
                {
                    continue;
                }
                var value = subComponents[1].Trim();
                switch (subComponents[0].Trim().ToUpperInvariant())
                {
                    case "C":
                        _country = Countries.Get(value);
                        break;
                    case "L":
                        Append(ref _city, value);
                        break;
                    case "DC":
                    case "O":
                        Append(ref _organization, value);
                        break;
                    case "OU":
                        Append(ref _organizationUnit, value);
                        break;
                    case "CN":
                        Append(ref _cn, value);
                        break;
                }
            }
        }

        private void Append(ref string? field, string value)
        {
            if (field == null)
            {
                field = value;
            }
            else
            {
                field += ", " + value;
            }
        }

        private CountryViewModel? _country = Countries.Get(RegionInfo.CurrentRegion.TwoLetterISORegionName);
        private string? _organization;
        private string? _organizationUnit;
        private string? _city;
        private string? _cn;
        private string? _issuer;
        private bool _hasChanges;
        private bool _selfSigned;
        private CertificateMode _mode;
        private DateTime _validFrom;
        private DateTime _validTo;
        private X509Certificate2? _certificate;

        public IList<CountryViewModel> AllCountries => Countries.AllCountries;

        public CountryViewModel? Country
        {
            get => _country;
            set => Set(ref _country, value);
        }

        public string? Organization
        {
            get => _organization;
            set => Set(ref _organization, value);
        }

        public string? OrganizationUnit
        {
            get => _organizationUnit; 
            set => Set(ref _organizationUnit, value);
        }

        public bool SelfSigned
        {
            get => _selfSigned;
            set => Set(ref _selfSigned, value);
        }

        public string? City
        {
            get => _city; set => Set(ref _city, value);
        }

        public CertificateMode Mode
        {
            get => _mode; set => Set(ref _mode, value);
        }

        public DateTime ValidFrom
        {
            get => _validFrom; set => Set(ref _validFrom, value);
        }

        public DateTime ValidTo
        {
            get => _validTo; set => Set(ref _validTo, value);
        }

        public string? CommonName
        {
            get => _cn;
            private set => Set(ref _cn, value);
        }

        public string? Issuer => _issuer;

        protected override bool Set<T>(ref T field, T value, [CallerMemberName] string? propertyName = null)
        {
            if( base.Set(ref field, value, propertyName))
            {
                return true;
            }
            return false;
        }

        public bool HasChanges
        {
            get => _hasChanges;
        }

        public bool IsPresent => _certificate != null;

        public void Create()
        {
            _hasChanges = true;
            GetOrCreateCertificate();
        }

        public string FillSubject(string commonName)
        {
            CommonName = commonName;
            var sb = new StringBuilder();
            sb.Append("CN=");
            sb.Append(commonName);
            if (Country?.Code != null)
            {
                sb.Append($",C={Country.Code}");
            }
            AppendAll("O", Organization, sb);
            AppendAll("OU", OrganizationUnit, sb);
            AppendAll("L", City, sb);
            return sb.ToString();
        }

        private void AppendAll(string key, string? value, StringBuilder sb)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return;
            }
            var entries = value.Split(',');
            foreach (var item in entries)
            {
                sb.Append($",{key}={item.Trim()}");
            }
        }

        public X509Certificate2 GetOrCreateCertificate()
        {
            if (_certificate == null || HasChanges)
            {
                _certificate = Mode.CreateCertificate(this);
                _hasChanges = false;
                OnPropertyChanged(nameof(HasChanges));
                SaveCertificate(true);
            }
            if (!_certificate.HasPrivateKey)
            {
                var ofd = new OpenFileDialog
                {
                    Title = "Select the key file",
                    Filter = "Key files (*.pem, *.key)|*.pem;*.key"
                };
                if (ofd.ShowDialog().GetValueOrDefault())
                {
                    var key = RSA.Create();
                    key.ImportFromPem(File.ReadAllText(ofd.FileName));
                    _certificate = _certificate.CopyWithPrivateKey(key);
                }
                else
                {
                    throw new InvalidOperationException("Key is required");
                }
            }
            return _certificate;
        }

        private void SaveCertificate(bool saveKey)
        {
            var sfd = new SaveFileDialog
            {
                Title = $"Select the location to save the {Mode.Desciption}",
                Filter = "PEM-encoded certificate files (*.pem, *.crt)|*.pem;*.crt|Pfx-encoded certificates (*.pfx)|*.pfx"
            };
            if (sfd.ShowDialog().GetValueOrDefault())
            {
                switch (sfd.FilterIndex)
                {
                    case 1:
                        SavePlaintextPem(sfd.FileName);
                        if (saveKey)
                        {
                            SaveKey();
                        }
                        break;
                    case 2:
                        SavePfx(sfd.FileName);
                        break;
                    default:
                        break;
                }
            }
        }

        private void SavePfx(string fileName)
        {
            File.WriteAllBytes(fileName, _certificate!.Export(X509ContentType.Pkcs12)); 
        }

        private void SaveEncryptedKey(string path)
        {
            var passwordWindow = new PasswordWindow();
            if (passwordWindow.ShowDialog() ?? false) {
                var password = passwordWindow.Password;
                File.WriteAllText(path, new string(PemEncoding.Write("ENCRYPTED PRIVATE KEY", 
                    _certificate!.GetRSAPrivateKey()!
                        .ExportEncryptedPkcs8PrivateKey(
                            password, 
                            new PbeParameters(PbeEncryptionAlgorithm.Aes256Cbc, HashAlgorithmName.SHA256, 10_000)))));
            }
        }

        private void SavePlaintextKey(string path)
        {
            File.WriteAllText(path, new string(PemEncoding.Write("PRIVATE KEY", _certificate!.GetRSAPrivateKey()!.ExportPkcs8PrivateKey())));
        }

        private void SavePlaintextPem(string path)
        {
            File.WriteAllText(path, new string(PemEncoding.Write("CERTIFICATE", _certificate!.GetRawCertData())));
        }

        private void SaveKey()
        {
            var sfd = new SaveFileDialog
            {
                Title = $"Select the location to save the key for the {Mode.Desciption}",
                Filter = "PEM-encoded plaintext key files (*.pem, *.key)|*.pem;*.key|PEM-encoded encrypted key files (*.pem, *.key)|*.pem;*.key"
            };
            if (sfd.ShowDialog().GetValueOrDefault())
            {
                switch (sfd.FilterIndex)
                {
                    case 1:
                        SavePlaintextKey(sfd.FileName);
                        break;
                    case 2:
                        SaveEncryptedKey(sfd.FileName);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("This should not happen");
                }
            }
        }
    }
}
