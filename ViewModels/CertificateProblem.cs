﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SiLACertificateGenerator.ViewModels
{
    public record CertificateProblem(string Message, ProblemSeverity Severity)
    {
        public Brush SeverityBrush => Severity switch
        {
            ProblemSeverity.Critical => Brushes.Red,
            ProblemSeverity.Information => Brushes.Green,
            ProblemSeverity.Warning => Brushes.Orange,
            _ => Brushes.Transparent
        };
    }
}
