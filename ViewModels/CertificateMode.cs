﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SiLACertificateGenerator.ViewModels
{
    public abstract class CertificateMode
    {
        public abstract X509Certificate2 CreateCertificate(CertificateViewModel certificate);

        public abstract string DefaultCommonName { get; }

        public abstract string Desciption { get; }

        public abstract Visibility AltNamesVisibility { get; }
    }
}
