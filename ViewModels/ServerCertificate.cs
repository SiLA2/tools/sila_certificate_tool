﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SiLACertificateGenerator.ViewModels
{
    public class ServerCertificate : CertificateMode
    {
        public ServerCertificate(Guid serverUuid)
        {
            ServerUuid = serverUuid;
        }

        public Guid ServerUuid { get; }

        public override string DefaultCommonName => "SiLA2";

        public override string Desciption => $"Certificate for server {ServerUuid}";

        public override Visibility AltNamesVisibility => Visibility.Visible;

        public override X509Certificate2 CreateCertificate(CertificateViewModel certificate)
        {
            var key = RSA.Create();
            var request = new CertificateRequest(certificate.FillSubject(DefaultCommonName), key, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);

            request.CertificateExtensions.Add(
                new X509BasicConstraintsExtension(false, false, 0, true));
            request.CertificateExtensions.Add(
                new X509KeyUsageExtension(
                    X509KeyUsageFlags.DigitalSignature | X509KeyUsageFlags.KeyEncipherment,
                    true));

            // add server uuid
            request.CertificateExtensions.Add(new X509Extension("1.3.6.1.4.1.58583", Encoding.ASCII.GetBytes(ServerUuid.ToString()), false));

            // SAN
            if (certificate.AlternativeNames.Any())
            {
                var builder = new SubjectAlternativeNameBuilder();
                foreach (var alternativeName in certificate.AlternativeNames)
                {
                    if (string.IsNullOrEmpty(alternativeName.Name))
                    {
                        continue;
                    }
                    if (IPAddress.TryParse(alternativeName.Name, out var alternativeIp))
                    {
                        builder.AddIpAddress(alternativeIp);
                    }
                    else
                    {
                        builder.AddDnsName(alternativeName.Name);
                    }
                }
                request.CertificateExtensions.Add(builder.Build());
            }
            // Enhanced key usages
            request.CertificateExtensions.Add(
                new X509EnhancedKeyUsageExtension(
                    new OidCollection {
                    new Oid("1.3.6.1.5.5.7.3.2"), // TLS Client auth
                    new Oid("1.3.6.1.5.5.7.3.1")  // TLS Server auth
                    },
                    false));
            // add this subject key identifier
            request.CertificateExtensions.Add(
                new X509SubjectKeyIdentifierExtension(request.PublicKey, false));

            // cert serial is the epoch/unix timestamp
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var unixTime = Convert.ToInt64((DateTime.UtcNow - epoch).TotalSeconds);
            var serial = BitConverter.GetBytes(unixTime);

            if (!certificate.SelfSigned)
            {
                var baseCertificate = MainViewModel.Instance.CertificateAuthority.GetOrCreateCertificate();
                return request
                    .Create(baseCertificate, DateTimeOffset.UtcNow.Subtract(TimeSpan.FromMinutes(1)), baseCertificate.NotAfter, serial)
                    .CopyWithPrivateKey(key);
            }
            else
            {
                return request.CreateSelfSigned(DateTimeOffset.UtcNow.Subtract(TimeSpan.FromMinutes(1)), certificate.ValidTo);
            }
        }
    }
}
