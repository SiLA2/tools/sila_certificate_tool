﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SiLACertificateGenerator.ViewModels
{
    public class CertificateAuthority : CertificateMode
    {
        public override string DefaultCommonName => "Root";

        public override string Desciption => "Certificate Authority (CA)";

        public override Visibility AltNamesVisibility => Visibility.Hidden;

        public override X509Certificate2 CreateCertificate(CertificateViewModel certificate)
        {
            var key = RSA.Create();
            var request = new CertificateRequest(certificate.FillSubject(certificate.CommonName ?? DefaultCommonName), key, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
            request.CertificateExtensions.Add(
                new X509BasicConstraintsExtension(true, false, 0, true));
            request.CertificateExtensions.Add(
                new X509KeyUsageExtension(
                    X509KeyUsageFlags.KeyCertSign,
                    true));
            return request.CreateSelfSigned(DateTime.Now.Subtract(TimeSpan.FromMinutes(1)), certificate.ValidTo);
        }
    }
}
