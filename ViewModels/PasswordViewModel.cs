﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiLACertificateGenerator.ViewModels
{
    internal class PasswordViewModel : ViewModelBase
    {
        private string? _password;

        public string? Password
        {
            get => _password;
            set => Set(ref _password, value);
        }
    }
}
