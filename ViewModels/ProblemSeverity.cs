﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiLACertificateGenerator.ViewModels
{
    public enum ProblemSeverity
    {
        Information,
        Warning,
        Critical
    }
}
