﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.Discovery;

namespace SiLACertificateGenerator.ViewModels
{
    public class ServerViewModel : ViewModelBase
    {
        private string _name;
        private ServerData _server;
        private static readonly DiscoveryExecutionManager _executionManager = new DiscoveryExecutionManager();

        public ServerViewModel(ServerData server)
        {
            _server = server;
            _name = server.Config.Name;
            Certificate = new CertificateViewModel(new ServerCertificate(server.Config.Uuid), null);
        }

        public CertificateViewModel Certificate { get; internal set; }

        public string Name
        {
            get => _name;
            set
            {
                if (Set(ref _name, value))
                {
                    var client = new SiLAServiceClient(_server.Channel, _executionManager);
                    client.SetServerName(value);
                }
            }
        }

        public string Type => _server.Info.Type;

        public string Description => _server.Info.Description;

        public string Version => _server.Info.Version;

        public string VendorUrl => _server.Info.VendorUri;

        public Guid ServerUuid => _server.Config.Uuid;

        public List<CertificateProblem> Problems { get; } = new List<CertificateProblem>();
    }
}
