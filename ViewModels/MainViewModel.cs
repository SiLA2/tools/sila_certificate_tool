﻿using Microsoft.Win32;
using SiLACertificateGenerator.Internal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Tecan.Sila2;
using Tecan.Sila2.Discovery;

namespace SiLACertificateGenerator.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private static MainViewModel? instance;

        private CertificateViewModel _ca = new(new CertificateAuthority(), null);
        private readonly CertificateAwareConnector _connector = new(new DiscoveryExecutionManager());
        private readonly IServerDiscovery _discovery;

        private string? _caPath;
        private ServerViewModel? _selectedServer;
        private CertificateViewModel _selectedCertificate;

        internal static MainViewModel Instance => instance!;

        private readonly DelegateCommand _loadCa;
        private readonly DelegateCommand _selectCa;
        private readonly DelegateCommand _scanNetwork;

        private int _scanNetworkDuration = 10;

        private string _scanNetworkFilter = "*";

        private string? _loadedCaPath;

        public MainViewModel()
        {
            instance = this;
            _selectedCertificate = _ca;
            _discovery = new ServerDiscovery(_connector);

            _loadCa = new DelegateCommand(LoadCa);
            _selectCa = new DelegateCommand(SelectCa);
            _scanNetwork = new DelegateCommand(ScanNetwork);
        }

        public ICommand LoadCaCommand => _loadCa;
        public ICommand SelectCaCommand => _selectCa;
        public ICommand ScanNetworkCommand => _scanNetwork;

        public CertificateViewModel CertificateAuthority
        {
            get => _ca;
            set => Set(ref _ca, value);
        }

        public ICollection<ServerViewModel> Servers { get; } = new ObservableCollection<ServerViewModel>();

        public void ScanNetwork()
        {
            Task.Run(() => _discovery.DiscoverServers(AddServer, TimeSpan.FromSeconds(ScanNetworkDuration), Networking.CreateNetworkInterfaceFilter(ScanNetworkFilter)));
        }

        private void AddServer(ServerData data)
        {
            var server = _connector.GetOrCreateViewModel(data);

            if (server.Certificate.HasChanges)
            {
                server.Problems.Add(new CertificateProblem("Server is not encrypted", ProblemSeverity.Critical));
            }

            if (!Servers.Contains(server))
            {
                App.Current.Dispatcher.Invoke(() => Servers.Add(server));
            }
        }

        public void LoadCa()
        {
            if (_loadedCaPath != _caPath && _caPath != null)
            {
                var extension = Path.GetExtension(_caPath)?.ToLowerInvariant();
                switch (extension)
                {
                    case ".crt":
                    case ".pem":
                    case ".pfx":
                        CertificateAuthority = new CertificateViewModel(_ca.Mode, new X509Certificate2(_caPath!));
                        break;
                    default:
                        throw new NotSupportedException($"Certificate extension {extension ?? "(none)"} is not supported!");
                }
                _loadedCaPath = _caPath;
            }
            SelectedCertificate = CertificateAuthority;
        }

        public void SelectCa()
        {
            var ofd = new OpenFileDialog
            {
                Title = "Select the path to the certificate file",
                Multiselect = false,
                Filter = "PEM-encoded certificates (*.pem, *.crt)|*.pem;*.crt|Pfx certificates (*.pfx)|*.pfx"
            };

            if (ofd.ShowDialog().GetValueOrDefault(false))
            {
                CaPath = ofd.FileName;
            }
            LoadCa();
        }

        public void SelectServer()
        {
            SelectedCertificate = SelectedServer?.Certificate ?? SelectedCertificate;
        }

        public string? CaPath
        {
            get => _caPath;
            set => Set(ref _caPath, value);
        }

        public string ScanNetworkFilter
        {
            get => _scanNetworkFilter;
            set => Set(ref _scanNetworkFilter, value);
        }

        public int ScanNetworkDuration
        {
            get => _scanNetworkDuration;
            set => Set(ref _scanNetworkDuration, value);
        }

        public ServerViewModel? SelectedServer
        {
            get => _selectedServer;
            set
            {
                if (Set(ref _selectedServer, value))
                {
                    SelectedCertificate = value?.Certificate ?? _ca;
                }
            }
        }

        public CertificateViewModel SelectedCertificate
        {
            get => _selectedCertificate;
            private set => Set(ref _selectedCertificate, value);
        }
    }
}
