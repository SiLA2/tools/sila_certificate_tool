﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiLACertificateGenerator.ViewModels
{
    public record CountryViewModel(string Name, string? Code)
    {
    }
}
