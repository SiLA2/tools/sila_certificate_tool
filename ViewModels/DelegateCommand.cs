﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SiLACertificateGenerator.ViewModels
{
    internal class DelegateCommand : ICommand
    {
        private readonly Action _action;

        public DelegateCommand(Action action)
        {
            _action = action;
        }

        public event EventHandler? CanExecuteChanged;

        public bool CanExecute(object? parameter)
        {
            return true;
        }

        public void Execute(object? parameter)
        {
            try
            {
                _action?.Invoke();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "An error happened", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
