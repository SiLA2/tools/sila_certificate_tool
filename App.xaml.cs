﻿using SiLACertificateGenerator.Internal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace SiLACertificateGenerator
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            Common.Logging.LogManager.Adapter = new DebugLogger();
            base.OnStartup(e);
        }
    }
}
