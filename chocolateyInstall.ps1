$path = Get-ChocolateyPath -PathType 'PackagePath'
$desktop = [Environment]::GetFolderPath("Desktop")
Install-ChocolateyShortcut -ShortcutFilePath "$desktop\SiLA Certificate Tool.lnk" -TargetPath "$path\tools\SiLACertificateGenerator.exe"