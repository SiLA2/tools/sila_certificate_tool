# SiLA2 Certificate Tool

This repository contains a tool to create certificates for SiLA2 servers. It scans the network for SiLA2 servers, displays the status of their certificates and allows the user to recreate new certificates and store them on the disk.

|||
| ---------------| ----------------------------------------------------------- |
| SiLA Homepage  | [https://sila-standard.com](https://sila-standard.com)      |
| Chat group     | [Join the group on Slack](https://sila-standard.org/slack)|
| Maintainer     | Georg Hinkel ([georg.hinkel@hs-rm.de](mailto:georg.hinkel@hs-rm.de)) of [Rhein-Main University of Applied Sciences](http://www.hs-rm.de)|

# Status

The repository is in a draft state. Certificates can be generated and saved as PKCS8 or PFX files. However, saving private keys as encrypted PEM currently does not work and there is no installer, yet.

## Contributing

To get involved, read our [contributing docs](https://gitlab.com/SiLA2/sila_base/blob/master/CONTRIBUTING.md) in [sila_base](https://gitlab.com/SiLA2/sila_base).

### C# Style Guide

Please follow the [general C# coding guidelines](https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/).